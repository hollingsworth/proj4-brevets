"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    error = False
    message = ""
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_date = request.args.get('begin_date', type=str)
    begin_time = request.args.get('begin_time', type=str)
    distance = request.args.get('distance', type = int)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    if type(km) != float or km < 0:
        error = True
        message = "Invalid input, enter a positive number"
    if (km - distance) > (.2*distance):
        message = "The last control point ({}km) is over 20% longer than the theoretical distance ({}km), \
        extent the brevet distance.".format(km, distance)

    str_datetime = str(begin_date) + ' ' + str(begin_time) + ":00"
    datetime = arrow.get(str_datetime, 'YYYY-MM-DD HH:mm:ss')

    app.logger.debug("datetime: {}".format(datetime))

    open_time = acp_times.open_time(km, distance, datetime.isoformat())
    close_time = acp_times.close_time(km, distance, datetime.isoformat())
    app.logger.debug("open_time: {}".format(open_time))
    app.logger.debug("close_time: {}".format(close_time))
    result = {"open": open_time, "close": close_time, "error": error, "message": message}
    return flask.jsonify(result=result)


#############

app.debug = True #CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
