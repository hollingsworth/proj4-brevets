# Project 4: Brevet time calculator with Ajax
Reimplement the RUSA ACP controle time calculator with flask and ajax.

## Author ##
Revised by Sydney Hollingsworth, sholling@uoregon.edu
Credits to Michal Young for the initial version of this code.

## Program Function
A calculator for ACP controle times. The algorithm used comes frmo Randonneurs USA, and is described here (https://rusa.org/pages/acp-brevet-control-times-calculator).
Built using AJAX in the frontend, and Flask in the backend, and packaged in a docker container.

## Test cases ## 
A test suite is provided, and can be implemented by running `nosetests` from within the brevets directory.

